//
//  BViewController.h
//  BlockTest
//
//  Created by wisdom on 16/3/10.
//  Copyright © 2016年 MY. All rights reserved.
//
#import <UIKit/UIKit.h>

typedef void(^CallBackBlcok) (NSString *text);

@interface BViewController : UIViewController

@property (nonatomic,copy)CallBackBlcok callBackBlock;

-(void)getBlock:(CallBackBlcok)block;

@end
