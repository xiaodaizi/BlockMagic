//
//  ViewController.m
//  BlockTest
//
//  Created by wisdom on 16/3/10.
//  Copyright © 2016年 MY. All rights reserved.
//

#import "AViewController.h"
#import "BViewController.h"
@interface AViewController ()

@end

@implementation AViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
}
- (IBAction)push:(id)sender {
    
    BViewController *bVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BViewController"];
    
    __weak AViewController *weakSelf = self;
    //将block 直接赋值
    
    bVC.callBackBlock = ^(NSString *text){
        
        NSLog(@"text is %@",text);
        
//        self.label.text = text;
        
        weakSelf.label.text = text;
        
    };
    //block 指针的说明例子
//    bVC.callBackBlock = ^(NSString *text){
//        
//        NSLog(@"text b is %@",text);
//
//    };
    
    //将block作为函数参数传递
    
//    [bVC getBlock:^(NSString *text) {
//        NSLog(@"text is %@",text);
//        
//        _label.text = text;
//    }];
    
    [self.navigationController pushViewController:bVC animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
