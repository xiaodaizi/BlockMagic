//
//  BViewController.m
//  BlockTest
//
//  Created by wisdom on 16/3/10.
//  Copyright © 2016年 MY. All rights reserved.
//

#import "BViewController.h"

@interface BViewController ()
@property (weak, nonatomic) IBOutlet UITextField *textField;

@end

@implementation BViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"BViewContorller";
    // Do any additional setup after loading the view.
}
- (IBAction)click:(id)sender {
    self.callBackBlock(_textField.text);
    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)getBlock:(CallBackBlcok)callBackBlock{
    
    self.callBackBlock = callBackBlock;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
